Title: Disable WSL system bell
Date: 2020-04-01 16:20
Modified: 2020-04-01 16:20
Category: Docs
Tags: notes, windows, wsl
Slug: disable-wsl-system-bell
Authors: Deyan Stoev
Summary: How to disable system bell in WSL

# Disable WSL system bell

To disable the beep in bash you need to uncomment (or add if not already there) the line set bell-style none in your /etc/inputrc file.

```bash
# do not bell on tab-completion
set bell-style none
set bell-style visible
```