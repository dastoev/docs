Title: Jenkins | Handling build abort
Date: 2020-02-11 18:40
Modified: 2020-04-01 16:20
Category: Jenkins
Tags: jenkins
Slug: jenkins-handling-build-abort
Authors: Deyan Stoev

# Handling build abort

## Make difference between timeout and user abort in scripted pipelines

```groovy
package com.dastoev.cicd.utils;

import jenkins.model.CauseOfInterruption
import org.jenkinsci.plugins.workflow.steps.FlowInterruptedException
import org.jenkinsci.plugins.workflow.steps.TimeoutStepExecution


public final class ExecutionHelper implements Serializable {

    static fixBuildResult(def script, def error){
        if (error instanceof FlowInterruptedException){
            if (error.getCauses().size() && error.getCauses().any { it instanceof CauseOfInterruption.UserInterruption }){
                script.echo "User cancelled the build"
                script.currentBuild.result = "ABORTED"
                script.currentBuild.description = 'Build aborted by user'
            }else if (error.getCauses().size() && error.getCauses().any { it instanceof TimeoutStepExecution.ExceededTimeout }){
                script.echo "Detected step timeout"
                script.currentBuild.result = "FAILURE"
                script.currentBuild.description = 'Step timeout'
            }else if (error.getCauses().size() && error.getCauses().any { it instanceof TimeoutStepExecution }){
                script.echo "Detected step timeout [backup]"
                script.currentBuild.result = "FAILURE"
                script.currentBuild.description = 'Step timeout'
            }else{
                script.echo "Unknown interuption"
                script.currentBuild.result = "FAILURE"
                script.currentBuild.description = 'Unknown interuption'
            }
        }else{
            script.currentBuild.result = "FAILURE"
            script.currentBuild.description = error.getMessage()
        }
    }
}
```
